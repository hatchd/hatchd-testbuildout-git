# README #

This repo holds Buildout configuration files that allow running coverage on the unit tests within a project. There is individual configurations for Pyramid/Python(as in no web framework is present) projects, Plone, and Django sites.

For Pyramid/Python sites, the test runner is nose, and for Plone sites the test runner is xmltestreport, which is the zope test runner, but with xml output support. Django sites should install the `django-nose` test runner.

Each configuration will add the following scripts into bin/buildout:

* `bin/test-coverage`

* `bin/generate-coverage-report`

* `bin/coverage`

* `bin/test` for Plone, and `bin/nosetests` for Pyramid/Python (no added script for Django)

NOTE: It's good practice to either pin the extends to a specific revision or the default branch. Changes to the buildout configs on the default branch SHOULD not break anything.

## Configuration ##
The following settings should be added to your projects [buildout] section.

### Required ###

**projectegg**(Plone only): The egg of your project

**projectdir**: The root directory of your project source files (not the root of the repository), relative to the root directory of your project.

If this is not configured correctly, the files not imported using your unit tests will not be present in the coverage, and python modules coverage might be measured too.

**testdir**: The directory of your projects tests, relative to the root directory of your project.

If this is not correct, your test_*.py files will show up in the coverage.

**coverage_eggs**: The eggs required to run your unittests. Generally using `${buildout:eggs}` as the coverage eggs will get the required eggs. **WARNING**: using `${instance:eggs}` on Plone sites causes issues.

### Optional ###

**additional_omits**: Any other files or directories to omit from the coverage report. This accepts both regex `PROJECT/dir_to_omit*` and individual files `PROJECT/file_to_omit.py`. Multiple omits should be comma seperated. E.G `additional_omits = PROJECT/dir_to_omit*,PROJECT/file_to_omit.py`

By default the **testdir** is ignored, along with **projectdir**/testing.py for the Plone projects.

### Pyramid/Python ###
add the following to your `buildout.cfg` file:


```
#!buildout

[buildout]
extends = https://bitbucket.org/hatchd/hatchd-testbuildout/raw/default/test_coverage_pyramid.cfg

projectdir = PROJECT_ROOT_DIRECTORY
testdir = PROJECT_TESTS_DIRECTORY
coverage_eggs = EGGS_REQUIRED_FOR_TESTING
additional_omits = ADDITIONAL_DIRS_OR_FILES_TO_OMIT

parts =
    ...
    test-coverage-scripts
    test-coverage
    generate-coverage-report
```

### Plone ###
add the following to your `buildout.cfg` file:

```
#!buildout

[buildout]
extends = https://bitbucket.org/hatchd/hatchd-testbuildout/raw/default/test_coverage_plone.cfg

projectegg = PROJECT_EGG
projectdir = PROJECT_ROOT_DIRECTORY
testdir = PROJECT_TESTS_DIRECTORY
coverage_eggs = EGGS_REQUIRED_FOR_TESTING
additional_omits = ADDITIONAL_DIRS_OR_FILES_TO_OMIT

parts =
    ...
    test-coverage-scripts
    test
    test-coverage
    generate-coverage-report
```


### Django ###
add the following to your `buildout.cfg` file:

```
#!buildout

[buildout]
extends = https://bitbucket.org/hatchd/hatchd-testbuildout/raw/default/test_coverage_django.cfg

projectdir = PROJECT_ROOT_DIRECTORY
testdir = PROJECT_TESTS_DIRECTORY
coverage_eggs = EGGS_REQUIRED_FOR_TESTING
additional_omits = ADDITIONAL_DIRS_OR_FILES_TO_OMIT

parts =
    ...
    test-coverage-scripts
    test-coverage
    generate-coverage-report
    
eggs =
    ...
    coverage
```

Add `django_nose` to your `INSTALLED_APPS` in `settings.py`:

```
#!python

INSTALLED_APPS = [
    ...
    'django_nose',
    ...
]
```

Set `TEST_RUNNER` in `settings.py`:
```
#!python
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
```


## Usage ##
To run the unit tests and capture the coverage (saved to .coverage file), from the project root, run:
```
#!bash
bin/test-coverage
```

To generate a coverage report from the .coverage file saved during the previous command:
```
#!bash
bin/generate-coverage-report
```